import { Component,OnInit } from '@angular/core';
import { SwapiService } from './swapi.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // declare models for this component
  values
  frmId:number = 1
  whichCategory:string = 'people'
  choices:Object[]  = [{cat:'people'},{cat:'planets'},{cat:'vehicles'},{cat:'species'},{cat:'starships'}]
  model = [{}]
  // declare functions
  constructor(private swapiService:SwapiService){ 
  }

  handleClick(){
    //invoke the service, passing parameters
    this.swapiService.getParamData(this.frmId,this.whichCategory)
    .subscribe((result)=>{this.model=result})

  }

  //call a method from the service
  invokeService(){
    this.swapiService.getData().subscribe( (result)=>{
      this.values = result
      console.log(result)} )
  }

  ngOnInit(){
    // make a call for user data
    this.invokeService()
  }

}
