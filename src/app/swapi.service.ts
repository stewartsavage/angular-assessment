import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {catchError} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class SwapiService {
  apiUrl:string = 'https://swapi.co/api/'

  constructor(private http:HttpClient) { }

  //gets json data from the specified API URL
  getData(){
    return this.http.get(`${this.apiUrl}`)
    .pipe(
      catchError(this.handleError<Object[]>('getData', []))
    );
  }

  //handles any error that occurs and sends a message to the console
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      console.error(error); // log to console instead
  
      // Let the app keep running by returning an empty result.
      return throwError(
        'Something bad happened, please try again later.'
      )
    };
  }


  //passes the id and category to the request to append to API address
  getParamData(id,category){
    return this.http.get<Object[]>(`${this.apiUrl}${category}/${id}`)
    .pipe(
      catchError(this.handleError<Object[]>('getParamData',[]))
    );
  }
}
